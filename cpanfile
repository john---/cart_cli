requires 'FindBin';
requires 'App::Cmd::Setup';
requires 'DBI';
requires 'DBD::Pg';
requires 'File::Slurp';
requires 'Time::Piece';
requires 'YAML::Tiny';